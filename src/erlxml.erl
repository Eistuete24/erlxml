-module(erlxml).
-export([parse/1]).
-export([find_values/2,find_key/5]).

parse(<<"<?xml",32,XML/bitstring>>) ->
	Data=meta(XML),
	parse(Data,[]);

parse(Data) when is_binary(Data) ->
	parse(Data,[]).

parse(<<>>,Lst)->
	Lst;
parse(<<"<",R/bitstring>>,Lst)->
	{R1,Tag,EndTag}=get_endtag_name(R,<<>>),
	{R2,Values}=get_element(R1,[],Tag,EndTag,bit_size(EndTag)),
	parse(R2,Lst++[{Tag,Values}]);

%%clear ws 
parse(<<_,R/bitstring>>,Lst) ->
	parse(R,Lst).

find_values(_Data,[]) -> 
	[];

find_values(<<"<?xml",32,XML/bitstring>>,KeyLst) ->
	Data=meta(XML),
	find_values(Data,KeyLst);

find_values(<<"<",Data/bitstring>>,KeyLst) ->
	find_values(Data,KeyLst,self()),
	receive 
		Lst -> Lst
	end;
%%clear ws 
find_values(<<_H,R/bitstring>>,Lst) -> 
	find_values(R,Lst).
%%/3
find_values(_,[],Pid) ->
	Pid ! []; 
%%allow for element and atttribute search!
find_values(Data,[{Element,AttrKeyVal}|Rest],PPid)->
	Bin=into_binary_element(AttrKeyVal,<<>>),
	Key= <<Element/bitstring,Bin/bitstring>>,
	Pid=spawn(?MODULE,find_key,[<<"<",Data/bitstring>>,[],Key,bit_size(Key),PPid]),
	find_values(Data,Rest,Pid);

find_values(Data,[Key|Rest],PPid) -> 
	Pid=spawn(?MODULE,find_key,[<<"<",Data/bitstring>>,[],Key,bit_size(Key),PPid]),
	find_values(Data,Rest,Pid).
into_binary_element([],Bin) ->
	Bin;
%%into one element
into_binary_element([{AttrKey,AttrV}|R],Bin) ->
	into_binary_element(R,<<Bin/bitstring,32,AttrKey/bitstring,"=",34,AttrV/bitstring,34>>).

find_key(<<>>,Lst,_,_,PPid) ->
	receive 
		[] -> PPid ! [Lst];
		Data -> PPid ! [Lst]++Data
	end;
%% same first letter
find_key(<<"<",H:8,R/bitstring>>,Lst,<<H:8,Key_rest/bitstring>>,KeySize,PPid) -> 
	%% is it really the key?
	case catch <<Key_rest:(KeySize-8)/bitstring,">",_/bitstring>> = R of
		{'EXIT',{_,_}} ->
			find_key(R,Lst,<<H:8,Key_rest/bitstring>>,KeySize,PPid);
		_ -> 
			{_,_,EndTag} = get_endtag_name(<<H:8,Key_rest/bitstring,">">>,<<>>),
			<<Key_rest:(KeySize-8)/bitstring,">",R1/bitstring>> =R,	
			{R2,Value}=get_element(R1,[],<<H,Key_rest/bitstring>>,EndTag,bit_size(EndTag)),
			find_key(R2,Lst++[{<<H:8,Key_rest/bitstring>>,Value}],<<H:8,Key_rest/bitstring>>,KeySize,PPid)
	end;

find_key(<<_H:8,R/bitstring>>,Lst,Key,KeySize,PPid)-> 
	find_key(R,Lst,Key,KeySize,PPid).


%%end tag?
get_element(<<"</",E:8,R/bitstring>>,Lst,_Tag,<<"</",E:8,En/bitstring>>,EndTagSize) ->
	<<En:(EndTagSize-24)/bitstring,R1/bitstring>> = R,
	{R1,Lst};
%% ignore ws
get_element(<<32,R/bitstring>>,Lst,Tag,EndTag,ETSize)->
	get_element(R,Lst,Tag,EndTag,ETSize);
%%ignore comments
get_element(<<"<!--",R/bitstring>>,Lst,Tag,EndT,ETSize)->
	R2=ignore(R),
	get_element(R2,Lst,Tag,EndT,ETSize);

get_element(<<"<![CDATA[",R/bitstring>>,Lst,Tag,EndT,ETSize)->
	{R1,CData}=cdata(R,<<>>,1),
	get_element(R1,Lst++CData,Tag,EndT,ETSize);

get_element(<<"<",R/bitstring>>,Lst,Tag,EndT,ETSize)->
	{R2,KeyVal}= case get_endtag_name(R,<<>>) of
			     {R1,[Tag1,_Attr],<<>>}  -> {R1,[]};
			     {R1,[Tag1,Attr],EndTag} ->
				     %% if there is no Attr == [] then empty list ist empty list,else tupleLst will get added
				     get_element(R1,Attr++[],Tag1,EndTag,bit_size(EndTag))
		     end,
	get_element(R2,Lst++[{Tag1,KeyVal}],Tag,EndT,ETSize);
%% clean ws 
get_element(<<10,R/bitstring>>,Lst,Tag,EndT,ETSize) -> 
	get_element(R,Lst,Tag,EndT,ETSize);
%%simple value!
get_element(<<H:8,R/bitstring>>,_Lst,_Tag,EndT,ETSize)->
	get_element_value(R,<<H>>,EndT,ETSize).

get_element_value(<<"</",E:8,R/bitstring>>,Bin,<<"</",E:8,En/bitstring>>,EndTagSize) ->
	<<En:(EndTagSize-24)/bitstring,R1/bitstring>> = R,
	{R1,Bin};

%% ignore comments
get_element_value(<<"<!--",R/bitstring>>,Bin,EndT,ETSize)->
	R2=ignore(R),
	get_element_value(R2,Bin,EndT,ETSize);
%%simple value
get_element_value(<<H:8,R/bitstring>>,Bin,EndT,ETSize) -> 
	get_element_value(R,<<Bin/bitstring,H>>,EndT,ETSize).
%% case there is an attribute
get_endtag_name(<<32,R/bitstring>>,Bin) ->
	{R2,Attr} = get_attribute(R,<<>>),
	{R2,[Bin,Attr],<<"</",Bin/bitstring,">">>};
%% is an endtag in itself <Tag/>
get_endtag_name(<<"/>",R/bitstring>>,Tag) -> 
	{R,[Tag,[]],<<>>};

get_endtag_name(<<">",R/bitstring>>,Bin) -> 
	{R,[Bin,[]],<<"</",Bin/bitstring,">">>};

get_endtag_name(<<H:8,R/bitstring>>,Bin) -> 
	get_endtag_name(R,<<Bin/bitstring,H>>).

ignore(<<"-->",R/bitstring>>) ->
	R;
ignore(<<_:8,R/bitstring>>)->
	ignore(R).
%% the number is character counter if needed 
%% increases, when in cdata are unallowed characters
cdata(<<"]]>",R/bitstring>>,Bin,1)->
	{R,Bin};

cdata(<<H:8,R/bitstring>>,Bin,N) ->
	cdata(R,<<Bin/bitstring,H>>,N).

meta(<<"?>",R/bitstring>>) -> 
	R;
meta(<<"version=",34,_Nbr:24/bitstring,34,32,"encoding=",34,R/bitstring>>)-> 
	meta(R);
meta(<<_:8,R/bitstring>>) -> 
	meta(R).

get_attribute(<<34,">",R/bitstring>>,Bin) -> 
	{R,Bin};
get_attribute(<<"=",R/bitstring>>,AttrKey) -> 
	{R1,AValue} = get_attribute(R,<<>>),
	{R1,[{AttrKey,AValue}]};

get_attribute(<<34,R/bitstring>>,Bin) -> 
	get_attribute(R,Bin);

get_attribute(<<H:8,R/bitstring>>,Bin) -> 
	get_attribute(R,<<Bin/bitstring,H>>).
