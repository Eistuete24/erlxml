# erlxml
An OTP library for parsing xml files into erlang terms. 
This lib is also able to find values, even with attributes.<br>
XHTML should also work for the Website-scraper in us.

xml alternative to json and inspired by 
https://github.com/gearnode/erl-json
### Usage
```
erlxml:parse(XMLData).
erlxml:find_values(XMLData,[XMLElement]).
erlxml:find_values(XMLData,[{XMLElement,[{AttributeKey,AttributeValue}]}]).
```

### rebar.config
```
{erlxml, ".*",{git, "https://gitlab.com/Eistuete24/erlxml,tag,"0.9.0"}}}
```
